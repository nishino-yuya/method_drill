package method_drill;

class Point{
	double x;
	double y;

	Point(double x, double y){
		this.x = x;
		this.y = y;
	}
}

public class Question22 {
	static double getDistanceFromOrigin(Point p) {
		double dfo = Math.sqrt((p.x * p.x) + (p.y * p.y));
		return dfo;
	}

	public static void main(String[] args) {
		// 動作確認
		// 点pの座標を乱数で設定
		double n = (double)(5 * Math.random());
		double m = (double)(5 * Math.random());
		// インスタンスの生成
		Point p = new Point(n, m);
		System.out.println(getDistanceFromOrigin(p));
	}

}
