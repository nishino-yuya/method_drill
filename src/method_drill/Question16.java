package method_drill;

public class Question16 {
	static double getMinValue(double a, double b) {
		double c;
		// b<aの時、二つの値を入れ替える処理をする
		if(a > b) {
			c = b;
			b = a;
			a = c;
		}
		return a;
	}

	public static void main(String[] args) {
		// 動作確認
		System.out.println(getMinValue(3.0, 2.0));
	}
}
