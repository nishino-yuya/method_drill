package method_drill;

public class Question9 {

	static void printMaxValue(double a, double b, double c) {
		if(a >= b && a>= c) {
			System.out.println(a);
		}else if(b >= c) {
			System.out.println(b);
		}else {
			System.out.println(c);
		}
	}

	public static void main(String[] args) {
		// 動作確認
		printMaxValue(0.1, 0.8, 0.9);
	}

}
