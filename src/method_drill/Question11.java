package method_drill;

public class Question11 {
	static String getWeatherForecast() {
		// 配列の宣言
		String[] days = {"今日", "明日", "明後日"};
		String[] weather = {"晴れ", "曇り", "雨", "雪"};

		//2つの乱数の宣言
		int n = (int)(3 * Math.random());
		int m = (int)(4 * Math.random());

		String fw = days[n] + "の天気は" + weather[m] + "です";
		return fw;
	}


	public static void main(String[] args) {
		// 動作確認
		System.out.println(getWeatherForecast());
	}

}
