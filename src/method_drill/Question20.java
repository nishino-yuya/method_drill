package method_drill;

public class Question20 {
	static double getAverage(double[] array) {
		double total =0;
		for(int i=0; i<array.length; i++) {
			total += array[i];
		}
		double ave = total / array.length;
		return ave;
	}

	public static void main(String[] args) {
		// 動作確認
		double[] numbers = {1.0, 2.0, 3.0};
		System.out.println(getAverage(numbers));
		}

}
