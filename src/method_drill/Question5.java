package method_drill;

public class Question5 {

	static void printCircleArea(double radius) {
		double ca = radius * radius * Math.PI;
		System.out.println(ca);
	}
	public static void main(String[] args) {
		// 動作確認
		printCircleArea(2.0);
	}

}
