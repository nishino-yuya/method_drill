package method_drill;

public class Question7 {
	static void printMessage(String name, int count) {
		for(int i=0; i<count; i++) {
			System.out.println(name);
		}
	}

	public static void main(String[] args) {
		// 動作確認
		printMessage("Hello", 5);
	}

}
