package method_drill;

public class Question17 {
	static boolean isSameAbsoluteValue(int i, int j) {
		// iとjが負の数なら、正の数に変換する
		if(i<0) {
			i *= -1;
		}
		if(j<0) {
			j *= -1;
		}
		// 等しいか確認し戻り値を返す
		if(i == j) {
			return true;
		}else {
			return false;
		}
	}

	public static void main(String[] args) {
		// 動作確認
		System.out.println(isSameAbsoluteValue(5, -5));
	}

}
