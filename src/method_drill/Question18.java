package method_drill;

public class Question18 {
	static String getMessage(String name, boolean isKid) {
		if(isKid == true) {
			String greet = "こんにちは。" + name + "さん。";
			return greet;
		}else {
			String greet = "こんにちは。" + name + "ちゃん。";
			return greet;
		}
	}

	public static void main(String[] args) {
		// 動作確認
		System.out.println(getMessage("西野", true));
	}

}
