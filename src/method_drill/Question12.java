package method_drill;

public class Question12 {

	static double getSquareRootOf2(double d) {
		double root = Math.sqrt(d);
		return root;
	}

	public static void main(String[] args) {
		// 動作確認
		System.out.println(getSquareRootOf2(2.0));
	}

}
