package method_drill;

public class Question14 {
	static double getAbsoluteValue(double value) {
		if(value >= 0.0) {
			return value;
		}else {
			return value * (-1);
		}
	}


	public static void main(String[] args) {
		// 動作確認
		System.out.println(getAbsoluteValue(-1.0));

	}
}
