package method_drill;

public class Question8 {
	static void printRectangleArea(double height, double width) {
		double ra = height * width;
		System.out.println(ra);
	}

	public static void main(String[] args) {
		// 動作確認
		printRectangleArea(1.0, 1.0);
	}

}
