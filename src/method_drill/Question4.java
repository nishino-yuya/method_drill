package method_drill;

public class Question4 {

	static void printMessage(String message) {
		System.out.println(message);
	}

	public static void main(String[] args) {
		// 動作確認
		printMessage("Hello");
	}

}
