package method_drill;

public class Question15 {
	static boolean isEvenNumber(int value) {
		if(value % 2 == 0) {
			return true;
		}else {
			return false;
		}
	}


	public static void main(String[] args) {
		// 0-10までの動作確認
		for(int i=0; i<=10; i++) {
			System.out.println(isEvenNumber(i));
		}
	}

}
