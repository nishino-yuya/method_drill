package method_drill;

public class Question24 {
	static Point getBarycenter(Point[] points) {
		double originX = 0;
		double originY = 0;
		for(int i=0; i<points.length; i++) {
			originX += points[i].x;
			originY += points[i].y;
		}
		Point orignP = new Point(originX, originY);
		return orignP;
	}

	public static void main(String[] args) {
		// 動作確認
		// インスタンスの作成
		Point p0 = new Point(2.0, 3.0);
		Point p1 = new Point(1.0, 2.0);

		// 配列の作成
		Point[] ps = {p0, p1};
		System.out.println(getBarycenter(ps));
		System.out.println(getBarycenter(ps).x);
		System.out.println(getBarycenter(ps).y);
	}
}

// Listを使えば作成したインスタンスを追加し、また新たなインスタンスの追加ができる