package method_drill;

public class Question21 {
	static String getLongestString(String[] array) {
		int strNum=0;
		String str = "";
		for(int i=0; i<array.length; i++) {
			if(strNum <= array[i].length()) {
				str = array[i];
			}
		}
		return str;
	}


	public static void main(String[] args) {
		// 動作確認
		String[] strs = {"こんにちは", "おはよう", "さよなら", "こんばんは"};
		System.out.println(getLongestString(strs));

	}

}
