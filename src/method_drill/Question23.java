package method_drill;

public class Question23 {
	static double getDistanceBetweenTwoPoints(Point p0, Point p1) {
		double dbtp = Math.sqrt((p0.x - p1.x) * (p0.x - p1.x) + (p0.y - p1.y) * (p0.y - p1.y));
		return dbtp;
	}

	public static void main(String[] args) {
		// 動作確認
		// 座標の乱数生成
		double n = (double)(5 * Math.random());
		double m = (double)(5 * Math.random());
		double l = (double)(5 * Math.random());
		double o = (double)(5 * Math.random());

		// インスタンスの生成
		Point p0 = new Point(n, m);
		Point p1 = new Point(l, o);

		System.out.println(getDistanceBetweenTwoPoints(p0, p1));
	}

}
