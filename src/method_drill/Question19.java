package method_drill;

public class Question19 {
	static int getMinValue(int array[]) {
		int number = array[0];
		for(int i=0; i<array.length; i++) {
			if(number < array[i]) {
				number = array[i];
			}
		}
		return number;
	}


	public static void main(String[] args) {
		// 動作確認
		int[] numbers = {5, 4, 3, 2};
		System.out.println(getMinValue(numbers));
	}

}
