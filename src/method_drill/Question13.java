package method_drill;
import java.util.ArrayList;

public class Question13 {
	static String getRandomMessage(String name) {
		ArrayList<String> greets = new ArrayList<String>();
		greets.add("こんばんは");
		greets.add("こんにちは");
		greets.add("おはよう");
		int n = (int)(3 * Math.random());
		String greet = greets.get(n) + " " + name + " " + "さん";
		return greet;

	}

	public static void main(String[] args) {
		// 動作確認
		System.out.println(getRandomMessage("西野"));
	}

}
